<?php
/**
 * Created by PhpStorm.
 * User: yuriybodak
 * Date: 10.07.17
 * Time: 18:49
 */

namespace Framework;


class OAuth extends Controller
{
    public static function checkUserSession(){
        if(!isset($_SESSION['user'])){
            self::redirect('User', 'login');
        }
    }
}