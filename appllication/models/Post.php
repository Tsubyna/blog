<?php

use Framework\Model;

class Post extends Model
{

    private $tableName = 'post';

    public function getAll(){
        $sql = "SELECT * FROM {$this->tableName} AS p INNER JOIN user AS u ON p.author = u.id  WHERE p.status='1' ORDER BY p.created_at DESC LIMIT 10";

        $query = $this->connect->prepare($sql);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public function getById($postId){
        $sql = "SELECT * FROM {$this->tableName} AS p INNER JOIN user AS u ON p.author = u.id INNER JOIN image AS i ON p.id = i.post_id  WHERE p.id=?";

        $query = $this->connect->prepare($sql);
        $query->execute([$postId]);
        $result = $query->fetch(PDO::FETCH_ASSOC);

        return $result;
    }


    public function save($data)
    {
        $sql = "INSERT INTO {$this->tableName} (title, text, author) VALUES (?, ?, ?)";
        $query = $this->connect->prepare($sql);
        $query->execute([
            $data['postTitle'],
            $data['postText'],
            $data['postAuthorId'],
        ]);

        $postId = $this->connect->lastInsertId();
        return $postId;
    }
}